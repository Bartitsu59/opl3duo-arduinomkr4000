#include "MIDIUSB.h"
#include <OPL3Duo.h>
#include <midi_instruments2_4op.h>
#include <midi_drums.h>

#define MT32MODE 0
#define MIDI_NUM_CHANNELS 16  
#define MIDI_DRUM_CHANNEL 10

struct OPL2Channel {
    byte note; 
    Instrument4OP instrument;           // Current instrument.
    byte program;                       // Program number currenly associated witht this MIDI channel.
    float volume;                       // Channel volume.
    float modulation;                   // Channel modulation.
    float afterTouch;                   // Channel aftertouch.
    unsigned long tAfterTouch;          // Aftertouch start.
};

const unsigned int notePitches[16] = {
	0x132, 0x144,
	0x156, 0x16B, 0x181, 0x198, 0x1B0, 0x1CA,
	0x1E5, 0x202, 0x220, 0x241, 0x263, 0x287,
	0x2AC, 0x2D6
};

OPL3Duo opl3Duo = OPL3Duo(2, 1, 0, 3, 4);
Instrument testInstrument[128];
Instrument drums[128];
int _mt32ToGm[128] = {
//	  0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F
	  0,   1,   0,   2,   4,   4,   5,   3,  16,  17,  18,  16,  16,  19,  20,  21, // 0x
	  6,   6,   6,   7,   7,   7,   8, 112,  62,  62,  63,  63,  38,  38,  39,  39, // 1x
	 88,  95,  52,  98,  97,  99,  14,  54, 102,  96,  53, 102,  81, 100,  14,  80, // 2x
	 48,  48,  49,  45,  41,  40,  42,  42,  43,  46,  45,  24,  25,  28,  27, 104, // 3x
	 32,  32,  34,  33,  36,  37,  35,  35,  79,  73,  72,  72,  74,  75,  64,  65, // 4x
	 66,  67,  71,  71,  68,  69,  70,  22,  56,  59,  57,  57,  60,  60,  58,  61, // 5x
	 61,  11,  11,  98,  14,   9,  14,  13,  12, 107, 107,  77,  78,  78,  76,  76, // 6x
	 47, 117, 127, 118, 118, 116, 115, 119, 115, 112,  55, 124, 123,   0,  14, 117  // 7x
};
OPL2Channel opl2Channels[MIDI_NUM_CHANNELS];

// First parameter is the event type (0x09 = note on, 0x08 = note off).
// Second parameter is note-on/note-off, combined with the channel.
// Channel can be anything between 0-15. Typically reported to the user as 1-16.
// Third parameter is the note number (48 = middle C).
// Fourth parameter is the velocity (64 = normal, 127 = fastest).

void noteOn(byte channel, byte pitch, byte velocity) {
  	// Calculate octave and note number.
	byte opl2Octave = 4;
	byte opl2Note = NOTE_C;

  pitch = max(24, min(pitch, 119));
  opl2Octave = 1 + (pitch - 24) / 12;
  opl2Note   = pitch % 12;
  opl2Channels[channel].note = opl2Note;
  // #ifndef MT32MODE
  //   // For notes on drum channel the note determines the instrument to load.
  //   if (channel == MIDI_DRUM_CHANNEL && opl2Note >= DRUM_NOTE_BASE && opl2Note < DRUM_NOTE_BASE + NUM_MIDI_DRUMS) {
  //     Serial.print("drums playing");
  //     const unsigned char *instrumentDataPtr = midiDrums[opl2Note - DRUM_NOTE_BASE];
  //     opl3Duo.setInstrument(channel, opl3Duo.loadInstrument(instrumentDataPtr));
  //   } else opl3Duo.setInstrument(channel, testInstrument[channel]);
  // #else
  //   if (channel == MIDI_DRUM_CHANNEL && opl2Note >= DRUM_NOTE_BASE && opl2Note < DRUM_NOTE_BASE + NUM_MIDI_DRUMS) {
  //     Serial.print("drums playing");
  //     const unsigned char *instrumentDataPtr = midiDrums[opl2Note - DRUM_NOTE_BASE];
  //     opl3Duo.setInstrument(channel, opl3Duo.loadInstrument(instrumentDataPtr));
  //   }else opl3Duo.setInstrument(channel, testInstrument[_mt32ToGm[channel]]);
  // #endif
  if (channel != MIDI_DRUM_CHANNEL) opl3Duo.playNote(channel, opl2Octave, opl2Note);
  Serial.print("Channel :");
  Serial.println(channel);
}

void noteOff(byte channel, byte pitch, byte velocity) {
	opl3Duo.setKeyOn(channel, false);
}

void setup() {
  Serial.begin(9600);
	opl3Duo.begin();
  for (int i=0;i<16;i++) {
    if (i==10) drums[i] = opl3Duo.loadInstrument(midiDrums[i]);
    else testInstrument[i] = opl3Duo.loadInstrument(midiInstruments[i]);
    opl3Duo.setInstrument(i, testInstrument[i]);
  }  
}

/**
 * Handle instrument change on the given MIDI channel and load the instrument if the MIDI channel is not the drum
 * channel where the note determines the instrument.
 */
void programChange(byte midiChannel, byte program) {
	midiChannel = midiChannel % 16;
	program = min(program, 127);
  #ifndef MT32MODE
    if (midiChannel!=MIDI_DRUM_CHANNEL) opl3Duo.setInstrument(midiChannel, testInstrument[program]);
  #else
    if (midiChannel!=MIDI_DRUM_CHANNEL) opl3Duo.setInstrument(midiChannel, testInstrument[_mt32ToGm[program]]);
  #endif
  Serial.print("Patch :");
  Serial.println(program);
  Serial.print(" - Midi channel :");
  Serial.println(midiChannel);
}

// First parameter is the event type (0x0B = control change).
// Second parameter is the event type, combined with the channel.
// Third parameter is the control number number (0-119).
// Fourth parameter is the control value (0-127).
void controlChange(byte channel, byte control, byte value) {
  midiEventPacket_t event = {0x0B, 0xB0 | channel, control, value};
  MidiUSB.sendMIDI(event);
}

/**
 * Apply a pitch bend to all notes currently playing on the given MIDI channel.
 * 
 * Note: Pitch bend is not applied to OPL channels that have a note in release, because when notes of different
 * frequencies are released changing their frequency may cause audible jitter.
 */
void onPitchChange(byte midiChannel, int pitch) {
	midiChannel = midiChannel % 16;
	float pitchBend = abs(pitch) / 8192.0;
  if (midiChannel != 10) {
    byte controlChannel = opl3Duo.get4OPControlChannel(midiChannel);
    byte baseNote = (opl2Channels[midiChannel].note % 12) + 2;

    if (pitch < 0) {
      byte fDelta = (notePitches[baseNote] - notePitches[baseNote - 2]) * pitchBend;
      opl3Duo.setFNumber(controlChannel, notePitches[baseNote] - fDelta);
    } else if (pitch > 0) {
      byte fDelta = (notePitches[baseNote + 2] - notePitches[baseNote]) * pitchBend;
      opl3Duo.setFNumber(controlChannel, notePitches[baseNote] + fDelta);
    } else {
      opl3Duo.setFNumber(controlChannel, notePitches[baseNote]);
    }
  }
}

void loop() {
  midiEventPacket_t rx;
  do {
    rx = MidiUSB.read();
    switch (rx.header) {
      case 0:
        break; //No pending events
        
      case 0x9:
        noteOn(
          rx.byte1 & 0xF,  //channel
          rx.byte2,        //pitch
          rx.byte3         //velocity
        );
        break;
        
      case 0x8:
        noteOff(
          rx.byte1 & 0xF,  //channel
          rx.byte2,        //pitch
          rx.byte3         //velocity
        );
        break;
        
      case 0xB:
        controlChange(
          rx.byte1 & 0xF,  //channel
          rx.byte2,        //control
          rx.byte3         //value
        );
        break;

      case 0xC:
        if(rx.byte1 == 10) {
          Serial.print("drums :");
          Serial.print(" - program :");
          Serial.print(rx.byte2 & 0xF, HEX);
        }

        programChange(
          rx.byte1 & 0xF,  //channel
          rx.byte2 & 0xF         // program
        );
        break;
      case 0xE:
      Serial.print("PITCH BENDING CHANNEL: ");
        onPitchChange(
          rx.byte1 & 0xF,  //channel
          (rx.byte2 & 0x7F) + 128 * (rx.byte3 & 0x7f)         //pitch
        );
        break;

      default:
        Serial.print("Unhandled MIDI message: ");
        Serial.print(rx.header, HEX);
        Serial.print("-");
        Serial.print(rx.byte1, HEX);
        Serial.print("-");
        Serial.print(rx.byte2, HEX);
        Serial.print("-");
        Serial.println(rx.byte3, HEX);
    }
  } while (rx.header != 0);
}
